package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        final TextView txtName=(TextView) findViewById(R.id.txtAnimalName);
        ImageView img=(ImageView) findViewById(R.id.imgView);

        final EditText edtEsperance=(EditText) findViewById(R.id.edtEsperance);
        final EditText edtPeriode=(EditText) findViewById(R.id.edtPeriode);
        final EditText edtPoids=(EditText) findViewById(R.id.edtPoids);
        final EditText edtPoids1=(EditText) findViewById(R.id.edtPoids1);
        final EditText edtStatut=(EditText) findViewById(R.id.edtStatut);

        Button btnSauvegard=(Button) findViewById(R.id.btnSauvegard);


        Intent intent=getIntent();


        String name=intent.getSerializableExtra("Name").toString();


        txtName.setText(name);

        final String image=intent.getSerializableExtra("img").toString();

        int id = getResources().getIdentifier("com.example.tp1:drawable/" +image , null, null);
        img.setImageResource(id);


        edtEsperance.setText(intent.getSerializableExtra("Lifespan").toString());
        edtPeriode.setText(intent.getSerializableExtra("Gestation").toString());
        edtPoids.setText(intent.getSerializableExtra("Birth").toString());
        edtPoids1.setText(intent.getSerializableExtra("AdultWeight").toString());
        edtStatut.setText(intent.getSerializableExtra("Conservation").toString());



        btnSauvegard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent1 = new Intent(getBaseContext(),MainActivity.class);

                intent1.putExtra("Name",txtName.getText());





                intent1.putExtra("Conservation",edtStatut.getText().toString());
                intent1.putExtra("img",image);

                try{
                    intent1.putExtra("Birth",Float.parseFloat(edtPoids.getText().toString()));
                }catch(Exception e){
                    Toast.makeText(getBaseContext(),"error",Toast.LENGTH_SHORT).show();
                }


                try{
                    intent1.putExtra("Gestation",Integer.parseInt(edtPeriode.getText().toString()));
                    intent1.putExtra("Lifespan",Integer.parseInt(edtEsperance.getText().toString()));

                    intent1.putExtra("AdultWeight",Integer.parseInt(edtPoids1.getText().toString()));
                }catch(Exception e){
                    Toast.makeText(getBaseContext(),"error",Toast.LENGTH_SHORT).show();
                }



                setResult(AnimalActivity.RESULT_OK,intent1);
                finish();
            }
        });

    }
}
