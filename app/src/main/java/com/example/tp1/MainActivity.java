package com.example.tp1;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    AnimalList animalList=new AnimalList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView=(ListView) findViewById(R.id.lst1);

        String animals[]=animalList.getNameArray();



        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,animals );
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String animalName = listView.getItemAtPosition(position).toString();
                Animal animal=AnimalList.getAnimal(animalName);

                Intent intent=new Intent(getBaseContext(),AnimalActivity.class);

                intent.putExtra("Name",animalName);

                intent.putExtra("AdultWeight",animal.getAdultWeight());
                intent.putExtra("Birth",animal.getBirthWeight());
                intent.putExtra("Conservation",animal.getConservationStatus());
                intent.putExtra("Gestation",animal.getGestationPeriod());
                intent.putExtra("Lifespan",animal.getHightestLifespan());
                intent.putExtra("img",animal.getImgFile());

                startActivityForResult(intent,1);





               // Toast.makeText(getBaseContext(),"aaaaaaa",Toast.LENGTH_SHORT).show();

            }
        });








    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {


        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {



                String name=data.getSerializableExtra("Name").toString();



                int lifespan= data.getIntExtra("Lifespan",0);


                int gestationPeriod= data.getIntExtra("Gestation",0);

                int adultWeight= data.getIntExtra("AdultWeight",0);

                String imgFile=data.getSerializableExtra("img").toString();

                String conservationStatus=data.getSerializableExtra("Conservation").toString();

                float birthWeight= data.getFloatExtra("Birth",0);





               Animal newAnimal=new Animal(lifespan,imgFile,gestationPeriod,birthWeight,adultWeight,conservationStatus);

               AnimalList.setAnimal(name,newAnimal);
            }
        }
        /**/
    }


}
